FROM python:3.6-alpine3.6

LABEL maintainer="James Hohman <jhohman@fitquisitor.com>"

# Add our build-time dependencies for removal later.
RUN apk add --update-cache --no-cache --virtual .build-time-dependencies \
    build-base \
    && pip install --no-cache-dir numpy~=1.15.0 \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    # Determine which packages are run-time dependencies
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .run-time-dependencies ${runDeps} \
    # Remove build-time dependencies
    && apk del .build-time-dependencies \
    && rm -rf /root/.cache
